'use strict';

const {
  DB_USERNAME,
  DB_PASSWORD,
  DB_NAME,
  DB_HOST,
  DB_PORT,
  DB_DIALECT,
  DB_SQLITE_LOCATION,
  DB_POOL_MAX,
  DB_POOL_MIN,
  DB_POOL_IDLE
} = require('./environment');

module.exports = {
  username: DB_USERNAME,
  password: DB_PASSWORD,
  database: DB_NAME,
  host: DB_HOST,
  port: DB_PORT,
  dialect: DB_DIALECT,
  storage: DB_SQLITE_LOCATION,
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  // TODO: connect this to winston logger
  logging: false
};
