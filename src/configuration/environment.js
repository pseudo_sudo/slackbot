'use strict';

// ALL ENVIRONMENTS VARIABLES SHOULD BE HANDLED HERE (process.env.*)
// This is to centralise the default settings and overrides
const path = require('path');
const chalk = require('chalk');
const dotenvConfig = require('dotenv').config();

// shim for es7 padEnd()
const padEnd = require('string.prototype.padend');
padEnd.shim();

// HELPERS
function environmentInteger(value, defaultValue) {
  if (value) {
    return parseInt(value, 10) || null;
  }

  return defaultValue;
}

// GENERAL
const NODE_ENV = process.env.NODE_ENV || 'development';
const PROJECT_ROOT_PATH = path.resolve(__dirname, '../..');
const APP_HTTP_SERVER_PORT = environmentInteger(process.env.APP_HTTP_SERVER_PORT, 8080);

// SLACK
const SLACK_API_TOKEN = process.env.SLACK_API_TOKEN || null;
const SLACK_BOT_NAME = process.env.SLACK_BOT_NAME || null;

// DATABASE
const DB_USERNAME = process.env.DB_USERNAME || null;
const DB_PASSWORD = process.env.DB_PASSWORD || null;
const DB_NAME = process.env.DB_NAME || null;
const DB_HOST = process.env.DB_HOST || null;
const DB_PORT = environmentInteger(process.env.DB_PORT, null);
const DB_DIALECT = process.env.DB_DIALECT || 'sqlite';
const DB_SQLITE_LOCATION = process.env.DB_SQLITE_LOCATION || 'tools/persistence/database.sqlite';
const DB_POOL_MAX = environmentInteger(process.env.DB_POOL_MAX, 5);
const DB_POOL_MIN = environmentInteger(process.env.DB_POOL_MIN, 0);
const DB_POOL_IDLE = environmentInteger(process.env.DB_POOL_IDLE, 10000);

// LOGGER
const LOG_FILE_PATH = process.env.LOG_FILE_PATH || 'tmp/application.log';

// SET ENVIRONMENT CONFIG
const environmentConfig = {
  NODE_ENV,
  PROJECT_ROOT_PATH,
  APP_HTTP_SERVER_PORT,
  SLACK_API_TOKEN,
  SLACK_BOT_NAME,
  DB_USERNAME,
  DB_PASSWORD,
  DB_NAME,
  DB_HOST,
  DB_PORT,
  DB_DIALECT,
  DB_SQLITE_LOCATION,
  DB_POOL_MAX,
  DB_POOL_MIN,
  DB_POOL_IDLE,
  LOG_FILE_PATH
};

// EXPORT ALL ENVIRONMENT CONFIG
module.exports = environmentConfig;
