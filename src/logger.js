'use strict';

const winston = require('winston');
const { Logger, transports } = winston;
const chalk = require('chalk');
const { LOG_FILE_PATH } = require('./configuration/environment');

const logger = new Logger({
  levels: winston.config.syslog.levels,
  transports: [
    new transports.Console({
      colorize: true
    }),
    new transports.File({
      filename: LOG_FILE_PATH,
      timestamp: true
    })
  ]
});

// TODO make this a transport mechanism
logger.environmentConfig = environmentConfig => {
  const envKeys = Object.keys(environmentConfig);

  // LOG USEFUL INFORMATION ABOUT ENVIRONMENT CONFIG
  console.log('\n');
  console.log(chalk.bold.underline('--- ENVIRONMENT CONFIGURATION  START ---'));

  envKeys.forEach(envKey => {
    const envValue = environmentConfig[envKey];
    const sensitiveKey =
      envKey.toUpperCase().includes('PASSWORD') ||
      envKey.toUpperCase().includes('API_KEY') ||
      envKey.toUpperCase().includes('API_TOKEN');
    const valueString =
      Boolean(envValue) && sensitiveKey
        ? String(envValue).replace(/./g, '*')
        : String(envValue);

    const valueStringColored = envValue
      ? valueString
      : chalk.yellow(valueString);

    console.log(`${envKey.padEnd(30)} --->    ${valueStringColored}`);
  });

  console.log('--- ENVIRONMENT CONFIGURATION  END ---');
  console.log('\n');
};

module.exports = {
  logger
};
