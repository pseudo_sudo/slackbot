'use strict';

const { Sequelize } = require('sequelize');
const sequelizeConfig = require('../configuration/persistence');
const { modelPaths } = require('./models');

function getModels() {
  const models = {};

  // Import Models
  modelPaths.forEach(file => {
    const model = sequelize.import(file);
    models[model.name] = model;
  });

  // Associate Models
  Object.keys(models).forEach(function(modelName) {
    if (models[modelName].associate) {
      models[modelName].associate(models);
    }
  });

  return models;
}

const sequelize = new Sequelize(
  sequelizeConfig.database,
  sequelizeConfig.username,
  sequelizeConfig.password,
  sequelizeConfig
);

const db = Object.assign(
  {
    connection: sequelize,
    sequelize: sequelize,
    Sequelize: Sequelize
  },
  getModels()
);

module.exports = { db };
