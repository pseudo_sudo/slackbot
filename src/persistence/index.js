'use strict';

const { logger } = require('../logger');
const { db } = require('./db');

function checkDB() {
  return new Promise((resolve, reject) => {
    db.connection
      .authenticate()
      .then(() => {
        logger.info('DB CONNECTED');
        resolve(db);
      })
      .catch(err => reject(`Unable to connect to the database: ${err}`));
  });
}

module.exports = {
  db,
  checkDB
};
