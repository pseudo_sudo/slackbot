'use strict';

const { logger } = require('./logger');
const { checkDB } = require('./persistence');
const { startBot } = require('./slackBot');
const { getAppServer } = require('./server');
const environmentConfig = require('./configuration/environment');
const {
  SLACK_API_TOKEN,
  SLACK_BOT_NAME,
  APP_HTTP_SERVER_PORT
} = environmentConfig;

function printEnvironmentConfig() {
  logger.environmentConfig(environmentConfig);
}

function initBot() {
  const botSettings = { token: SLACK_API_TOKEN, name: SLACK_BOT_NAME };
  return startBot(botSettings);
}

async function initServer(config) {
  const app = await getAppServer(config);
  const port = APP_HTTP_SERVER_PORT;
  app.listen(port);
  logger.info(`WEBSERVER STARTED on port ${port}`);
  return app;
}

async function initialize() {
  await printEnvironmentConfig();
  const db = await checkDB();
  const bot = await initBot();
  await initServer({ bot, db });

  return true;
}

initialize()
  .then(() => {
    logger.info('APPLICATION STARTED');
  })
  .catch(err => {
    logger.crit(err);

    process.exit(1);
  });
