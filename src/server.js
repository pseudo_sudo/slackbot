const Koa = require('koa');
const Router = require('koa-router');

function buildRouter() {
  const router = new Router();

  router.get('/', (ctx, next) => {
    ctx.body = 'Hello World';
  });

  router.get('/dashboard', (ctx, next) => {
    ctx.body = 'TODO Dashboard';
  });

  return router;
}

function getAppServer({ bot, db }) {
  const app = new Koa();
  const router = buildRouter();

  app.use(router.routes()).use(router.allowedMethods());

  return Promise.resolve(app);
}

module.exports = {
  getAppServer
};
