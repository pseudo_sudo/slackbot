'use strict';

const Bot = require('slackbots');
const { logger } = require('./logger');

module.exports = {
  startBot
};

function startBot({ token, name }) {
  const slackBot = new Bot({ token, name });

  return new Promise((resolve, reject) => {
    slackBot.on('message', event => {
      // logger.info('slackBot: event fired, when something happens in Slack. Description of all events here');
      logger.info(event.content || event);
    });

    slackBot.on('open', () => {
      logger.info('SLACK CONNECTED');

      // slackBot.postMessageToChannel('test-channel', 'Hello channel!');
      // slackBot.postMessageToUser('pseudosudo', 'hello friend!');
      // slackBot.postMessageToGroup('test-channel', 'hello group chat!');

      resolve(slackBot);
    });

    slackBot.on('close', () => {
      logger.log('slackBot: websocket connection is closed');
    });

    slackBot.on('error', () => {
      reject(new Error('an error occurred while connecting to Slack'));
    });
  });
}
